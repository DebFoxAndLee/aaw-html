<?php include_once('inc/header.php'); ?>
<div class="content tracking ptb-5em">
  <div class="container">
    <div class="intro_head text-center">
      <div class="dash-line"></div><h1 class="big-title text-left">Competitive quotes <br />start here</h1>
    </div>
  </div>

  <div class="request-quote pt-5em">
    <div class="left_section_quote">
      <div class="blue_box">
        <div class="blueBoxContent">
          <header>request a quote</header>
          <p>We specialise in providing end to end supply chain logistic services, catering for all requirements from collection of cargo through to delivery at destination.</p>
          <p class="white_colour">We can provide a quote for any size project and taylor it to suit your requirements precisely.</p>
          <img src="img/quote_svg.png" alt="">
        </div>
      </div>
      <div class="quote_image">
      </div>
    </div>
    <div class="right_section_quote">
      <div class="quote_form">
        <div class="tracker_login">
          <form action="">
            <fieldset>
              <label for="">Company Code</label>
              <input type="text" name="" value="">
            </fieldset>
            <fieldset>
              <label for="">Email</label>
              <input type="email" name="">
            </fieldset>
            <fieldset>
              <label for="">Password</label>
              <input type="password" name="">
            </fieldset>
            <fieldset>
              <input type="checkbox" name="">
              <span>Remember Me</span>
            </fieldset>
            <fieldset>
              <input type="submit" class="button" name="" value="submit">
            </fieldset>
            <fieldset>
              <a class="forgot_pw" href="#">Forgotten your password?</a>
            </fieldset>
          </form>
        </div>
      </div>
    </div>



  </div>



</div>
<?php include_once('inc/footer.php'); ?>
