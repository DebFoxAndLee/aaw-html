<?php include_once('inc/header.php'); ?>
<div class="content services pt-5em pb-3em">
  <div class="container">
    <div class="clearfix top_intro_head">
      <div class="col-md-12 contact_head_title">
        <h1 class="big-title">Contact Us</h1>
      </div>
    </div>
    <div class="service-showcase pt-4em pb-2em clearfix">
      <div class="col-md-3 column_left">
        <div class="contact_office_list services_lists">
          <header>our offices</header>
          <div class="city_list_dropdown btn-group">
            <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Choose Location <i class="fa fa-angle-down ml-8"></i></button>
            <div class="dropdown-menu">
              <a href="#">Melbourne</a>
              <a href="#">Sydney</a>
              <a href="#">Brisbane</a>
              <a href="#">Adelaide</a>
              <a href="#">Fremantle</a>
              <a href="#">Townsville</a>
              <a href="#">Darwin</a>
              <a href="#">Mildura</a>
              <a href="#">Launceston</a>
            </div>
          </div>
          <ul class="city-lists">
            <li class="current-cat"><a href="#">Melbourne</a></li>
            <li><a href="#">Sydney</a></li>
            <li><a href="#">Brisbane</a></li>
            <li><a href="#">Adelaide</a></li>
            <li><a href="#">Fremantle</a></li>
            <li><a href="#">Townsville</a></li>
            <li><a href="#">Darwin</a></li>
            <li><a href="#">Mildura</a></li>
            <li><a href="#">Launceston</a></li>
          </ul>
        </div>
      </div>
      <div class="col-md-9 contact_column_right">
        <div class="contact_infos">
          <div class="row clearfix">
            <div class="col-sm-5">
              <div class="office-location-name"><header>Melbourne</header></div>
              <div class="is_head_office">Head Office</div>
              <div class="address-blocks-repeat">
                <div class="add_blocks">
                  <header>Street Address</header>
                  <p>Level 1, 102 Dodds Street, <br />Southbank, Vic 300</p>
                </div>
                <div class="add_blocks">
                  <header>Postal Address</header>
                  <p>P.O. Box 952 <br />South Melbourne, 3205</p>
                </div>
                <div class="add_blocks">
                  <header>Telephone</header>
                  <p>61-3-9611 6860</p>
                </div>
                <div class="add_blocks">
                  <header>Fax</header>
                  <p>61-3-9611 6860</p>
                </div>
                <div class="add_blocks">
                  <header>State Manager</header>
                  <p>Karen Chernikeeff</p>
                </div>
                <div class="add_blocks">
                  <header>Email</header>
                  <p><a href="mailto:k.chernikeeff@aawglobal.com.au">k.chernikeeff@aawglobal.com.au</a></p>
                </div>
              </div>
            </div>
            <div class="col-sm-7"><img src="img/map.jpg" alt=""></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php include_once('inc/footer.php'); ?>
