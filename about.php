<?php include_once('inc/header.php'); ?>
<div class="content about_us_page pt-5em">
  <div class="container">
    <div class="intro_head text-center">
      <div class="dash-line"></div><h1 class="big-title text-left">We Are Committed To <br />Excellent Customer Service.</h1>
    </div>
  </div>

  <div class="about-banner ptb-5em relative">
    <div class="about-blue-left"></div>
    <div class="about-bannerimage-right"></div>
    <div class="container clearfix">
      <div class="col-md-5 banner_text_style">
        <header>OUR TEAM IS WHAT SETS APART FROM THE REST</header>
        <h2>We have over 60 staff in main offices and employ 200 staff Australia wide. Our dedicated team operate out of 5 fully operational offices and 4 additional representative offices.</h2>
        <p>We are proud to provide the highest quality service and advice at a personalized level, which is exemplified by our motto “Commitment to Service “. It is this philosophy that allows us to appreciate that every customer has its own special demands.</p>
      </div>
    </div>
    <div class="visible-sm visible-xs staff_pic_odd"><img src="img/staff-pic-1.jpg" alt="Staff Pic"></div>
  </div>

  <div class="about-team pt-5em pb-3em">
    <div class="container">
      <div class="col-sm-3 col-md-2">
        <div class="services_lists"><header>EXCEPTIONAL <br />SERVICE</header></div>
      </div>
      <div class="col-sm-9 col-md-10">
        <div class="team-info-content">
          <h2>Our market coverage includes an international network that encompasses all facets of transportation and logistics that enables us to provide our customers a “Total Global Multi-Model Service”.</h2>
          <p>It is the range of services that we can offer on a global scale that makes us stand out from the rest.  We also provide services tailored to individual customers, such as transport and distribution, warehousing and storage, pre-retailing services, dedicated transport, project management and consultancy.</p>
        </div>
      </div>
    </div>
  </div>

  <div class="wholeteam text-center"><img src="img/wholeteam.jpg" alt=""></div>
  <div class="teamimage slanting_blue">
    <div class="container">
      <div class="center_two_box">
        <div class="ship-plane">
          <span><img src="img/boat-plane.svg" alt="boat and plane" width="90"></span>
          <span class="txt">We have moved over <strong>576,000</strong> tonnes of sea and freight goods.</span>
        </div>
        <div class="two-docks">
          <span><img src="img/boxes-X2.svg" alt="two docks" width="90"></span>
          <span class="txt">We have over <strong>550,000</strong> square metres of warehousing storage space worldwide.</span>
        </div>
      </div>
    </div>
  </div>


  <div class="teaminfo ptb-4em">

    <div class="team_repeat_row">
      <div class="container">
        <div class="col-md-2">
          <div class="services_lists"><header>BRANCH <br />MANAGERS</header></div>
        </div>
        <div class="col-md-10 toggle_team">
          <div class="row clearfix">
            <div class="col-sm-4 col-md-4 single_team">
              <a href="#">
                <figure style="background-image:url('img/manager.png');"></figure>
                <div class="teamnames">
                  <header>Scott Last Name</header>
                  <span>Title to go here</span>
                </div>
              </a>
            </div>
            <div class="col-sm-4 col-md-4 single_team">
              <a href="#">
                <figure style="background-image:url('img/manager.png');"></figure>
                <div class="teamnames">
                  <header>Scott Last Name</header>
                  <span>Title to go here</span>
                </div>
              </a>
            </div>
            <div class="col-sm-4 col-md-4 single_team">
              <a href="#">
                <figure style="background-image:url('img/manager.png');"></figure>
                <div class="teamnames">
                  <header>Scott Last Name</header>
                  <span>Title to go here</span>
                </div>
              </a>
            </div>
          </div>
        </div>
      </div><!-- container -->
      <div class="togglecontent_team">hello world</div>
    </div>

    <div class="team_repeat_row">
      <div class="container">
        <div class="col-md-2 box_for_title">
          <div class="services_lists"></div>
        </div>
        <div class="col-md-10 toggle_team">
          <div class="row clearfix">
            <div class="col-sm-4 col-md-4 single_team">
              <a href="#">
                <figure style="background-image:url('img/manager.png');"></figure>
                <div class="teamnames">
                  <header>Scott Last Name</header>
                  <span>Title to go here</span>
                </div>
              </a>
            </div>
            <div class="col-sm-4 col-md-4 single_team">
              <a href="#">
                <figure style="background-image:url('img/manager.png');"></figure>
                <div class="teamnames">
                  <header>Scott Last Name</header>
                  <span>Title to go here</span>
                </div>
              </a>
            </div>
            <div class="col-sm-4 col-md-4 single_team">
              <a href="#">
                <figure style="background-image:url('img/manager.png');"></figure>
                <div class="teamnames">
                  <header>Scott Last Name</header>
                  <span>Title to go here</span>
                </div>
              </a>
            </div>
          </div>
        </div>
      </div><!-- container -->
      <div class="togglecontent_team">hello world</div>
    </div>

    <div class="team_repeat_row">
      <div class="container">
        <div class="col-md-2 box_for_title">
          <div class="services_lists"></div>
        </div>
        <div class="col-md-10 toggle_team">
          <div class="row clearfix">
            <div class="col-sm-4 col-md-4 single_team">
              <a href="#">
                <figure style="background-image:url('img/manager.png');"></figure>
                <div class="teamnames">
                  <header>Scott Last Name</header>
                  <span>Title to go here</span>
                </div>
              </a>
            </div>
            <div class="col-sm-4 col-md-4 single_team">
              <a href="#">
                <figure style="background-image:url('img/manager.png');"></figure>
                <div class="teamnames">
                  <header>Scott Last Name</header>
                  <span>Title to go here</span>
                </div>
              </a>
            </div>
          </div>
        </div>
      </div><!-- container -->
      <div class="togglecontent_team">hello world</div>
    </div>

  </div> <!-- team info -->


  <div class="about-second-banner ptb-5em relative">
    <div class="visible-sm visible-xs staff_pic_odd"><img src="img/staff-pic-2.jpg" alt="Staff Pic"></div>
    <div class="second-banner-image"></div>
    <div class="about-blue-right"></div>
    <div class="container clearfix">
      <div class="col-md-7"></div>
      <div class="col-md-5 banner_text_style">
        <header>DEDICATED PEOPLE WITH SPECIALIST SKILLS</header>
        <h2>Whether it is a small carton of
product through to a complete
industrial plant, AAW has the
specialist skills, dedicated people
and international resources to
facilitate a seamless flow to/from
any worldwide destination.</h2>
        <header class="pt-1em"><a href="#">VIEW OUR SERVICES <i class="fa fa-angle-right ml-8"></i><i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></a></header>
      </div>
    </div>
  </div> <!--about second banner -->



  <div class="teaminfo ptb-4em">

    <div class="team_repeat_row">
      <div class="container">
        <div class="col-md-2">
          <div class="services_lists"><header>CORPORATE <br />TEAM</header></div>
        </div>
        <div class="col-md-10 toggle_team">
          <div class="row clearfix">
            <div class="col-sm-4 col-md-4 single_team">
              <a href="#">
                <figure style="background-image:url('img/manager.png');"></figure>
                <div class="teamnames">
                  <header>Scott Last Name</header>
                  <span>Title to go here</span>
                </div>
              </a>
            </div>
            <div class="col-sm-4 col-md-4 single_team">
              <a href="#">
                <figure style="background-image:url('img/manager.png');"></figure>
                <div class="teamnames">
                  <header>Scott Last Name</header>
                  <span>Title to go here</span>
                </div>
              </a>
            </div>
            <div class="col-sm-4 col-md-4 single_team">
              <a href="#">
                <figure style="background-image:url('img/manager.png');"></figure>
                <div class="teamnames">
                  <header>Scott Last Name</header>
                  <span>Title to go here</span>
                </div>
              </a>
            </div>
          </div>
        </div>
      </div><!-- container -->
      <div class="togglecontent_team">hello world</div>
    </div>

    <div class="team_repeat_row">
      <div class="container">
        <div class="col-md-2 box_for_title">
          <div class="services_lists"></div>
        </div>
        <div class="col-md-10 toggle_team">
          <div class="row clearfix">
            <div class="col-sm-4 col-md-4 single_team">
              <a href="#">
                <figure style="background-image:url('img/manager.png');"></figure>
                <div class="teamnames">
                  <header>Scott Last Name</header>
                  <span>Title to go here</span>
                </div>
              </a>
            </div>
            <div class="col-sm-4 col-md-4 single_team">
              <a href="#">
                <figure style="background-image:url('img/manager.png');"></figure>
                <div class="teamnames">
                  <header>Scott Last Name</header>
                  <span>Title to go here</span>
                </div>
              </a>
            </div>
            <div class="col-sm-4 col-md-4 single_team">
              <a href="#">
                <figure style="background-image:url('img/manager.png');"></figure>
                <div class="teamnames">
                  <header>Scott Last Name</header>
                  <span>Title to go here</span>
                </div>
              </a>
            </div>
          </div>
        </div>
      </div><!-- container -->
      <div class="togglecontent_team">hello world</div>
    </div>



  </div> <!-- team info -->






</div>
<?php include_once('inc/footer.php'); ?>
