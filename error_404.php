<?php include_once('inc/header.php'); ?>
<div class="content error_404 pt-6em">

<div class="aaw-error text-center">
  <img src="img/aaw-404.svg" alt="404 error">
</div>
<div class="error_message">
  <div class="txt-error-msg">
    <header>Error Message</header>
    <p>The page you are looking for does not seem to be avialable.
Click back or <a href="#">click here</a> to go to the home page</p>
  </div>
</div>


</div>
<?php include_once('inc/footer.php'); ?>
