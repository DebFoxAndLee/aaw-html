<?php include_once('inc/header.php'); ?>
<div class="content services pt-5em pb-3em">
  <div class="container">
    <div class="clearfix top_intro_head">
      <div class="col-md-3 column_left">
        <div class="train-boat relative">
          <img src="img/train-boat-plane.svg" alt="">
        </div>
      </div>
      <div class="col-md-9 column_right">
        <h1 class="big-title services_head">A full range of import and export services.</h1>
      </div>
    </div>
    <div class="service-showcase pt-4em pb-2em clearfix">
      <div class="col-md-3 column_left">
        <div class="services_lists">
          <header>Services</header>
          <ul>
            <li class="current-cat"><a href="#">Freight Forwarding</a></li>
            <li><a href="#">Breakbulk</a></li>
            <li><a href="#">Domestic transport</a></li>
            <li><a href="#">Bulk Liquid Logistics</a></li>
            <li><a href="#">Supply Chain Management</a></li>
            <li><a href="#">Customers Brokerage </a></li>
            <li><a href="#">Shipping Documentation</a></li>
            <li><a href="#">Warehousing & Fulfilment </a></li>
          </ul>
        </div>
        <div class="request_quote_button relative">
          <div class="quote_btn">
            <header>Request <br />a Quote <br />Today</header>
            <a href="" class="button white">BEGIN</a>
          </div>
        </div>
      </div>
      <div class="col-md-9 column_right">
        <div class="services-content-box">
          <h2>Freight Forwarding</h2>
          <div class="service_content_block">
            <h3>Seafreight Export:</h3>
            <p>AAW provides a full range of export services that includes:</p>
              <ul>
                <li>Seafreight  (Full containers and Part)</li>
                <li>Cargo Transport</li>
                <li>Export clearance and Pre Receival Advices</li>
                <li>Container Packing</li>
                <li>Consolidations</li>
                <li>Full export documentation including Bills of Lading, Certificates of Origin and more.</li>
              </ul>
            <p>Our extensive global network allows AAW to provide exports services to any destination worldwide.  We can also arrange for goods to be customs cleared and delivered to your customers door for most destinations.</p>

            <h3>Seafreight Import:</h3>
            <p>AAW provides a full range of import services from all regions worldwide.  Our extensive global network gives us the ability to manage import shipments from just about any location.  We specialise in providing a fully managed logistics service catering for all requirements from the pick up of cargo at origin all the way through to cargo delivery in Australia.  Our range of services includes:</p>
            <ul>
              <li>Seafreight (full containers and LCL)</li>
              <li>Airfreight</li>
              <li>Consolidations</li>
              <li>Cargo transport</li>
              <li>Warehousing and Distribution</li>
              <li>Order Tracking with full internet access</li>
              <li>Electronic Documentation storage</li>
            </ul>

            <h3>Air Freight:</h3>
            <p>As a Regulated Air Cargo Agent & IATA Accredited, AAW is ideally suited to handle those important and urgent airfreight deliveries.  Our services include:</p>
            <ul>
              <li>International and Domestic Airfreight</li>
              <li>Local transport</li>
              <li>Full airfreight documentation</li>
              <li>Full delivery through door to door</li>
              <li>Full or part aircraft charters to handle very large project shipments</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php include_once('inc/footer.php'); ?>
