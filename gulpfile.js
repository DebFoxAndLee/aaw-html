var gulp = require('gulp');
var autoprefixer = require('gulp-autoprefixer'); // auto prefixer for css
var browserSync = require('browser-sync').create();


gulp.task('browser-sync', function() {
	"use strict";
    browserSync.init({
        proxy: "http://localhost/aaw-html"
    });
	gulp.watch(['./css/style.css', './*.php', './**/*.js']).on('change', browserSync.reload);

});

gulp.task('autoprefixer', function(){
	"use strict";
	return gulp.src('css/style.css')
		.pipe(autoprefixer({ browsers: ["> 0%"] }))
		.pipe(gulp.dest('css/'));
});
