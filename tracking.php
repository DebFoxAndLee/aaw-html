<?php include_once('inc/header.php'); ?>
<div class="content tracking ptb-5em">
  <div class="container">
    <div class="intro_head text-center">
      <div class="dash-line"></div><h1 class="big-title text-center">Real-time any time</h1>
    </div>
  </div>

  <div class="tracking-content_box pt-4em">
    <div class="container clearfix">
      <div class="col-md-6 web_tracker_text">
        <div class="tracker_blue_box">
          <header>web tracker</header>
          <p>Web Tracker delivers 24/7 access to tracking information through a feature-rich online portal. The seamless integration provides total visibility to monitor the status and movements of goods in real-time at any time.</p>
          <img src="img/boxes-X4.svg" alt="" width="216">
        </div>
      </div>
      <div class="col-md-6 tracker_login">
        <form action="">
          <fieldset>
            <label for="">Company Code</label>
            <input type="text" name="" value="">
          </fieldset>
          <fieldset>
            <label for="">Email</label>
            <input type="email" name="">
          </fieldset>
          <fieldset>
            <label for="">Password</label>
            <input type="password" name="">
          </fieldset>
          <fieldset>
            <input type="checkbox" name="">
            <span>Remember Me</span>
          </fieldset>
          <fieldset>
            <input type="submit" class="button" name="" value="submit">
          </fieldset>
          <fieldset>
            <a class="forgot_pw" href="#">Forgotten your password?</a>
          </fieldset>
        </form>
      </div>
    </div>
  </div>

  <div class="main-tracking-content pt-4em">
    <div class="container clearfix">
      <div class="col-md-6">
        <div class="tracking_left_content">
          <header>Web Tracker’s customizable interface enables a tailor-made track and trace service</header>
          <ul>
          	<li>Track Orders & Shipments
          	<li>Make a Booking</li>
          	<li>Place an Order</li>
          	<li>Manage Containers</li>
          	<li>Get a New Quotation</li>
          	<li>View Schedules</li>
          	<li>Access Documentation</li>
          	<li>Locate Invoices & Statements</li>
          	<li>Generate Reports</li>
          	<li>Audit Compliance</li>
          </ul>
          <span>Please <a href="#">contact your local AAW Global representative</a> for further information or to provide you with your log in details for this service.</span>
          <div class="how-to-videos">
            <a href="#">View Web Trackers<br />how-to videos</a>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="tracking-image"><img src="img/boat-ocean-pic.jpg" alt="Boat Ocean"></div>

      </div>
    </div>
  </div>

</div>
<?php include_once('inc/footer.php'); ?>
