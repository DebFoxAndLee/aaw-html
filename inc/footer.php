<footer class="main-footer pt-4em pb-2em">
  <div class="container clearfix">
    <div class="col-sm-7 col-md-4 footer_widget">
      <div class="block_our_services">
        <h3>OUR SERVICES</h3>
        <ul>
          <li><a href="#">Freight Forwarding</a></li>
          <li><a href="#">Project forwarding / Breakbulk</a></li>
          <li><a href="#">Domestic Transport - Ship Rail Road</a></li>
          <li><a href="#">Bulk Liquid Logistics</a></li>
          <li><a href="#">Supply Chain Management</a></li>
          <li><a href="#">Customers Brokerage</a></li>
          <li><a href="#">Shipping Documentation</a></li>
          <li><a href="#">Warehousing & Fulfilment</a></li>
        </ul>
      </div>
    </div>
    <div class="col-sm-5 col-md-3 footer_widget">
      <div class="block_locations">
        <h3>locations</h3>
        <ul>
          <li><a href="#">Melbourne</a></li>
          <li><a href="#">Sydney</a></li>
          <li><a href="#">Brisbane</a></li>
          <li><a href="#">Adelaide</a></li>
          <li><a href="#">Fremantle</a></li>
          <li><a href="#">Townsville</a></li>
          <li><a href="#">Darwin</a></li>
          <li><a href="#">Mildura</a></li>
          <li><a href="#">Launceston</a></li>
        </ul>
      </div>
    </div>
    <div class="col-sm-12 col-md-5 footer_widget">
      <div class="block_tracking">
        <h3>tracking</h3>
        <div class="go-tracking">
          <p>Login and track your goods here</p>
          <a href="" class="button">GO TO TRACKING PAGE</a>
        </div>
        <div class="copyright pt-5em">
          <div class="copy-left">
            <h4>AAW GLOBAL LOGISTICS &copy; <?php echo date('Y'); ?></h4>
            <div class="footer-terms"><a href="#">Terms & Conditions</a><a href="#">Disclaimer</a><a href="#">Privacy Policy</a></div>
          </div>
          <div class="footer-logo">
            <img src="img/aaw-logo-white.svg" alt="AAW Global" width="152">
          </div>
        </div>
      </div>
    </div>
    <div class="col-sm-12 site_by">
      <a target="_blank" href="http://foxandlee.com.au">Website by Fox & Lee</a>
    </div>
  </div>
</footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/full_video.js"></script>
<script src="js/custom.js"></script>
<script src="js/rangeslider.min.js"></script>
</body>
</html>
