<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>AAW Global</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/rangeslider.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body id="<?=basename($_SERVER['PHP_SELF'],'.php')?>">
    <div class="fade-overlay"></div>
    <div class="video_holder">
        <div id="container">
          <video id="background_video" loop muted></video>
          <div id="video_cover"></div>
        </div>
        <div class="navigation pt-3em relative">
          <nav class="navbar">
            <div class="container">
              <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"><img src="img/aaw-logo-white.svg" alt="AAW Global" width="152" class="homeonly"><img src="img/aaw-logo-blue.svg" class="otherpage" width="152" alt="AAW Global"></a>
              </div>
              <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                  <li><a href="#">Home</a></li>
                  <li><a href="#">About us</a></li>
                  <li><a href="#">Services <span class="caret"></span></a></li>
                  <li><a href="#">Tracking</a></li>
                  <li><a href="#">Request a quote</a></li>
                  <li><a href="#">Contact</a></li>
                  <li><a href="#" class="showphone"><i class="fa fa-phone"></i></a></li>
                </ul>
              </div><!--/.nav-collapse -->
            </div>
          </nav>
        </div>
        <div class="text_land_sea_air">
          <div class="centertxt">
            <h1>LAND SEA AIR</h1>
            <span class="helper_txt">LOGISTIC SOLUTIONS WITH <strong>VISIBILITY</strong> </span>
            <span class="color_bar"></span>
          </div>
        </div>
        <div class="downarrow"><img src="img/down-arrow.png" alt=""></div>
    </div>

    <div class="slidewrapper">
      <div class="close-wrapper-box"><a href="" class="close_slide_wrapper"><i class="fa fa-times" aria-hidden="true"></i></a></div>
      <div class="heading_quick_contact"><strong>Quick <br />Contact</strong></div>
      <ul class="contact-list-box">
        <li>
          <span class="office_location"><a href="#">Melbourne</a></span>
          <span class="office_tel"><a href="tel:">+61 3 9611 6860</a></span>
          <span class="link_to_contact"><a href="#">View office Details</a></span>
        </li>
        <li>
          <span class="office_location"><a href="#">Melbourne</a></span>
          <span class="office_tel"><a href="tel:">+61 3 9611 6860</a></span>
          <span class="link_to_contact"><a href="#">View office Details</a></span>
        </li>
        <li>
          <span class="office_location"><a href="#">Melbourne</a></span>
          <span class="office_tel"><a href="tel:">+61 3 9611 6860</a></span>
          <span class="link_to_contact"><a href="#">View office Details</a></span>
        </li>
        <li>
          <span class="office_location"><a href="#">Melbourne</a></span>
          <span class="office_tel"><a href="tel:">+61 3 9611 6860</a></span>
          <span class="link_to_contact"><a href="#">View office Details</a></span>
        </li>
        <li>
          <span class="office_location"><a href="#">Melbourne</a></span>
          <span class="office_tel"><a href="tel:">+61 3 9611 6860</a></span>
          <span class="link_to_contact"><a href="#">View office Details</a></span>
        </li>
        <li>
          <span class="office_location"><a href="#">Melbourne</a></span>
          <span class="office_tel"><a href="tel:">+61 3 9611 6860</a></span>
          <span class="link_to_contact"><a href="#">View office Details</a></span>
        </li>
        <li>
          <span class="office_location"><a href="#">Melbourne</a></span>
          <span class="office_tel"><a href="tel:">+61 3 9611 6860</a></span>
          <span class="link_to_contact"><a href="#">View office Details</a></span>
        </li>
      </ul>
    </div>
